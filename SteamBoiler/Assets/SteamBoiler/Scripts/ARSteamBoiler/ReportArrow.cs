﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReportArrow : MonoBehaviour
{
    [Header("Current")]
    public MeshRenderer currentMR = null;
    public Transform currentParrent = null;
    public Transform circleTrOnUI = null;

    public void SetupInit(Transform parent)
    {
        transform.name = "Arrow";

        currentParrent = parent;
        transform.parent = parent;

        currentMR = parent.GetComponent<MeshRenderer>();
        Vector3 center = currentMR.bounds.center;
        transform.position = new Vector3(center.x, center.y + currentMR.bounds.extents.y, center.z);

        transform.localRotation = Quaternion.Euler(180, 90, 0);        
        transform.localScale = Vector3.one;
    }

    [Header("UI View")]    
    public Transform circlePrefab = null;    
    private Transform _holder = null;
    private Transform holder
    {
        get
        {
            if (_holder == null)
            {
                CanvasArrow canvasArrow = FindObjectOfType<CanvasArrow>();
                if (canvasArrow != null) _holder = canvasArrow.arrowHolder;
            }

            return _holder;
        }
    }

    [ContextMenu("UpdateCircleInUI")]
    public void UpdateCircleInUI()
    {
        if (holder == null) return;
        if (circleTrOnUI == null)
        {
            circleTrOnUI = Instantiate(circlePrefab, Vector3.zero, Quaternion.identity, holder);
            circleTrOnUI.name = currentParrent.name;

            btnShowIntroPopupAfterClick btnUI = circleTrOnUI.gameObject.AddComponent<btnShowIntroPopupAfterClick>();
            btnUI.currentName = currentParrent.name;
        }
                
        Vector3 center = transform.TransformPoint(currentMR.bounds.center);
        circleTrOnUI.position = Camera.main.WorldToScreenPoint(center);
    }

    void RegisterCircleInUI()
    {
        ThirdPersonCamera tpCamera = FindObjectOfType<ThirdPersonCamera>();
        tpCamera.dellateUpdate += UpdateCircleInUI;
    }

    void UnRegisterCircleInUI()
    {
        try
        {
            ThirdPersonCamera tpCamera = FindObjectOfType<ThirdPersonCamera>();
            tpCamera.dellateUpdate -= UpdateCircleInUI;

            if (circleTrOnUI != null) if (Application.isPlaying) Destroy(circleTrOnUI.gameObject); else DestroyImmediate(circleTrOnUI.gameObject);
        }
        catch { }
    }

    private void OnEnable()
    {
        RegisterCircleInUI();
    }

    private void OnDisable()
    {
        UnRegisterCircleInUI();
    }

    private void OnDestroy()
    {
        UnRegisterCircleInUI();
    }
}
