﻿using SteamBoiler.tPart.ARSteamBoiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetExplain : MonoBehaviour
{
    [Header("Params")]
    public SteamBoilerDatabase data = null;

    [Header("Input")]
    public string boilerName = string.Empty;
    public string partName = string.Empty;
    public int index = -1;

    [Header("Output")]
    public string strExplain = string.Empty;
    public Sprite sprExplain = null;
    public GameObject goExplain = null;
    
    [ContextMenu("DoGet")]
    public void DoGet()
    {
        data.GetExplain(boilerName, partName, index, out strExplain);
        data.GetExplain(boilerName, partName, index, out sprExplain);
        data.GetExplain(boilerName, partName, index, out goExplain);
    }
}
