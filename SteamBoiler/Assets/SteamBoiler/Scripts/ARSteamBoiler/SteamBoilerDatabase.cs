﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SteamBoiler.tPart.ARSteamBoiler
{

    #region SteamBoiler Define

    [Serializable]
    public class ASteamBoiler
    {
        public string imageName = string.Empty;
        public GameObject prefab = null;

        public List<BoilerPart> boilerParts = new List<BoilerPart>();
        public BoilerPart GetBoilerPart(string partName) { return boilerParts.Find((x) => x.partName == partName); }

        public ASteamBoiler() { }
    }

    /// <summary>
    /// Case Iext : Show text to UI
    /// Case Image : 
    /// </summary>
    [Serializable]
    public enum IntroType { text, image, gameobject }

    [Serializable]
    public class PartExplain
    {
        public IntroType typeIntro = IntroType.text;
        public string partIntro;
    }

    [Serializable]
    public class BoilerPart
    {
        public string partName;
        public List<PartExplain> partList = new List<PartExplain>();
        public bool isClickable;
    }

    #endregion

    #region Inner Define

    public enum eLanguage { English, Japanese }

    [Serializable]
    public class AnInnerPartTextExplain
    {
        public eLanguage language = eLanguage.English;
        public string explain = string.Empty;
        public AnInnerPartTextExplain() { }
        public AnInnerPartTextExplain(eLanguage language, string explain) { this.language = language; this.explain = explain; }
    }

    [Serializable]
    public class AnInnerPartText
    {
        public string partName = string.Empty;
        public List<AnInnerPartTextExplain> textList = new List<AnInnerPartTextExplain>();
        public AnInnerPartText()
        {
            textList = new List<AnInnerPartTextExplain>();
            textList.Add(new AnInnerPartTextExplain(eLanguage.English, string.Empty));
            textList.Add(new AnInnerPartTextExplain(eLanguage.Japanese, string.Empty));
        }

        public string GetExplain(eLanguage language) { return textList.Find((x) => x.language == language).explain; }
    }

    [Serializable]
    public class AnInnerPartImage
    {
        public string partName = string.Empty;
        public Sprite sprite = null;
        public AnInnerPartImage() { }
    }

    [Serializable]
    public class AnInnerPartGameObject
    {
        public string partName = string.Empty;
        public GameObject prefab = null;
        public AnInnerPartGameObject() { }
    }

    #endregion

    [CreateAssetMenu(fileName = "SteamBoilerDatabase", menuName = "SteamBoiler/SteamBoilerDatabase")]
    public class SteamBoilerDatabase : ScriptableObject
    {
        [Header("Steam Database")]
        public List<ASteamBoiler> boilerList = new List<ASteamBoiler>();

        public ASteamBoiler GetASteamBoiler(string imageName) { return boilerList.Find((x) => x.imageName == imageName); }

        [Header("Part Database")]
        public eLanguage currentLanguage = eLanguage.English;

        public List<AnInnerPartText> innerTextList = new List<AnInnerPartText>();
        public AnInnerPartText GetInnerText(string partName) { return innerTextList.Find((x) => x.partName == partName); }

        public List<AnInnerPartImage> innerSpriteList = new List<AnInnerPartImage>();
        public AnInnerPartImage GetInnerSprite(string partName) { return innerSpriteList.Find((x) => x.partName == partName); }

        public List<AnInnerPartGameObject> innerGOList = new List<AnInnerPartGameObject>();
        public AnInnerPartGameObject GetInnerGO(string partName) { return innerGOList.Find((x) => x.partName == partName); }

        bool CheckGetExplain(string boilerName, string partName, int index, out PartExplain explainOut)
        {
            ASteamBoiler boiler = GetASteamBoiler(boilerName);
            if (boiler == null) { explainOut = null; return false; }

            BoilerPart boilerPart = boiler.GetBoilerPart(partName);
            if (boilerPart == null) { explainOut = null; return false; }

            PartExplain explain = null;
            try
            {
                explain = boilerPart.partList[index];
            }
            catch
            {
                explainOut = null;
                return false;
            }

            explainOut = explain;
            return true;
        }

        public void GetExplain(string boilerName, string partName, int index, out string Explain)
        {
            PartExplain explain = null;
            if (!CheckGetExplain(boilerName, partName, index, out explain))
            {
                Explain = string.Empty;
                return;
            }

            Explain = explain.typeIntro == IntroType.text ? explain.partIntro : string.Empty;
        }

        public void GetExplain(string boilerName, string partName, int index, out Sprite Explain)
        {
            PartExplain explain = null;
            if (!CheckGetExplain(boilerName, partName, index, out explain))
            {
                Explain = null;
                return;
            }

            Explain = explain.typeIntro == IntroType.image ? GetInnerSprite(explain.partIntro).sprite : null;
        }

        public void GetExplain(string boilerName, string partName, int index, out GameObject Explain)
        {
            PartExplain explain = null;
            if (!CheckGetExplain(boilerName, partName, index, out explain))
            {
                Explain = null;
                return;
            }

            Explain = explain.typeIntro == IntroType.gameobject ? GetInnerGO(explain.partIntro).prefab : null;
        }
    }
}