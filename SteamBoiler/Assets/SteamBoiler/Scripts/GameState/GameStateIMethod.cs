﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SteamBoiler.tPart.GameState
{
    public interface GameStateIMethod
    {
        void OnClick();
    }
}