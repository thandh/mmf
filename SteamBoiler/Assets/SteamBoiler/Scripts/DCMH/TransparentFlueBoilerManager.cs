﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentFlueBoilerManager : MonoBehaviour {
    public List<Transform> flueBoilerPart = new List<Transform>();
    public bool isTrans;

    private void Start()
    {
        isTrans = false;
        Transform[] parts = GetComponentsInChildren<Transform>();
        foreach(Transform p in parts)
        {
            flueBoilerPart.Add(p);
        }
    }

    //Set Transparent or Standard for all part in boiler
    void SetOuter(bool Set)
    {
        if (flueBoilerPart.Count == 0) return;

        ChangeMode changeMode = GetComponent<ChangeMode>();

        foreach (Transform o in flueBoilerPart)
        {
            MeshRenderer mr = o.gameObject.GetComponent<MeshRenderer>();
            if (mr != null)
            {
                changeMode.mr = mr;
                Material newMaterial = Set ? changeMode.GetOpaque() : changeMode.GetTransparent();
                mr.material = newMaterial;

                Color currentColor = mr.material.color;

                //mr.material.SetFloat("_Mode", Set ? 0f : 3f);
                mr.material.color =
                    new Color(currentColor.r, currentColor.g, currentColor.b, Set ? 1 : 0);

                //mr.UpdateGIMaterials();
            }
        }
    }

    //Change Outer Boiler Onclick
    [ContextMenu("ChangeOuterBoiler")]
    public void ChangeOuterBoiler()
    {
        SetOuter(isTrans);
        isTrans = !isTrans;
    }
}
