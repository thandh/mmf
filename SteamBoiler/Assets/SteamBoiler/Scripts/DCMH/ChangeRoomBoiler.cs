﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeRoomBoiler : MonoBehaviour {
    public GameObject OnceThroughBoiler;
    public GameObject FlueBoiler;

    //Change From OnceThroughBoiler Room to FlueBoiler Room & opposite
    [ContextMenu("ChangeRoom")]
    public void ChangeRoom()
    {
        OnceThroughBoiler.SetActive(!OnceThroughBoiler.activeSelf);
        FlueBoiler.SetActive(!FlueBoiler.activeSelf);
    }
}
