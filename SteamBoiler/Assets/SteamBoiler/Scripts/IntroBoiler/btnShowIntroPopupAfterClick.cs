﻿using SteamBoiler.tPart.ARSteamBoiler;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class btnShowIntroPopupAfterClick : MonoBehaviour
{
    [Header("Params")]
    public string currentName = string.Empty;

    Button _introSelect = null;
    Button introSelect
    {
        get
        {
            if (_introSelect == null) _introSelect = GetComponent<Button>();
            return _introSelect;
        }
    }

    public void OnEnable()
    {
        introSelect.onClick.AddListener(ActiveIntroPopup);
    }

    void ActiveIntroPopup()
    {
        CanvasSubWindow subwindow = FindObjectOfType<CanvasSubWindow>();
        if (string.IsNullOrEmpty(subwindow.currentBoiler.imageName))
            return;

        ASteamBoiler currentBoiler = subwindow.database.GetASteamBoiler(subwindow.currentBoiler.imageName);
        BoilerPart boilerPart = currentBoiler.GetBoilerPart(currentName);

        subwindow.panelTr.gameObject.SetActive(true);
        subwindow.GenerateExplain(currentBoiler, boilerPart);
    }

    public void OnDisable()
    {
        introSelect.onClick.RemoveListener(ActiveIntroPopup);
    }
}
