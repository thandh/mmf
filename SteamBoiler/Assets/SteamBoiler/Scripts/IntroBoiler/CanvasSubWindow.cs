﻿using System;
using SteamBoiler.tPart.ARSteamBoiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSubWindow : MonoBehaviour
{
    [Header("Inputs")]
    public Button btnClose = null;
    public Transform panelTr = null;
    public Transform contentTr = null;
    public Transform viewportTr = null;
    public ScrollRect scrollRect = null;

    public SteamBoilerDatabase database;
    public SteamBoilerScriptable currentBoiler;

    private void Start()
    {
        CheckCurrentLanguage();
    }

    [Header("Language")]
    public Button btnEnglish = null;
    public Button btnJapanese = null;

    void CheckCurrentLanguage()
    {
        btnEnglish.gameObject.SetActive(database.currentLanguage != eLanguage.English);
        btnJapanese.gameObject.SetActive(!btnEnglish.gameObject.activeSelf);
    }

    public void ChangeLanguage(string e)
    {
        database.currentLanguage = (eLanguage)Enum.Parse(typeof(eLanguage), e);
    }

    [ContextMenu("ToggleCurrentLanguage")]
    public void ToggleCurrentLanguage()
    {
        database.currentLanguage = database.currentLanguage == eLanguage.English ? eLanguage.Japanese : eLanguage.English;
    }

    [Header("Explain Inputs")]
    public GameObject subTextPrefab = null;
    public GameObject subSpritePrefab = null;
    public GameObject subGOPrefab = null;

    [Header("Explain Process")]
    public bool doneExplain = true;
    public bool doneAnExplain = true;

    public void GenerateExplain(ASteamBoiler boiler, BoilerPart part)
    {
        StartCoroutine(C_GenerateExplain(boiler, part));
    }

    IEnumerator C_GenerateExplain(ASteamBoiler boiler, BoilerPart part)
    {
        doneExplain = false;

        for (int i = 0; i < part.partList.Count; i++)
        {
            string currentText = string.Empty;
            Sprite currentSprite = null;
            GameObject currentGO = null;

            database.GetExplain(boiler.imageName, part.partName, i, out currentText);
            database.GetExplain(boiler.imageName, part.partName, i, out currentSprite);
            database.GetExplain(boiler.imageName, part.partName, i, out currentGO);

            GenerateAnExplain(currentText, currentSprite, currentGO);
            yield return new WaitUntil(() => doneAnExplain == true);
        }

        doneExplain = true;
        yield break;
    }

    void GenerateAnExplain(string txtExplain = "", Sprite sprExplain = null, GameObject goExplain = null)
    {
        StartCoroutine(C_GenerateAnExplain(txtExplain, sprExplain, goExplain));
    }

    IEnumerator C_GenerateAnExplain(string txtExplain = "", Sprite sprExplain = null, GameObject goExplain = null)
    {
        doneAnExplain = false;

        if (string.IsNullOrEmpty(txtExplain) && sprExplain == null && goExplain == null) { doneExplain = true; yield break; }

        if (!string.IsNullOrEmpty(txtExplain) && (sprExplain == null && goExplain == null))
        {
            subText subtext = Instantiate(subTextPrefab, Vector3.zero, Quaternion.identity, contentTr).GetComponent<subText>();
            Text text = subtext.GetComponentInChildren<Text>();
            text.text = txtExplain;
        }
        else if (sprExplain != null && (string.IsNullOrEmpty(txtExplain) && goExplain == null))
        {
            subSprite subsprite = Instantiate(subSpritePrefab, Vector3.zero, Quaternion.identity, contentTr).GetComponent<subSprite>();
            subsprite.image.sprite = sprExplain;
        }
        else if (goExplain != null && (string.IsNullOrEmpty(txtExplain) && sprExplain == null))
        {
            Transform subGO = Instantiate(subGOPrefab, Vector3.zero, Quaternion.identity, contentTr).transform;
            Transform GO = Instantiate(goExplain, subGO).transform;
            GO.localPosition = Vector3.zero; GO.localRotation = Quaternion.identity;
        }

        doneAnExplain = true;
        yield break;
    }

    [Header("Delete")]
    public GameObject contentSubWinPrefab = null;

    [ContextMenu("DeleteContent")]
    public void DeleteContent()
    {
        if (contentTr == null) return;
        if (Application.isPlaying) Destroy(contentTr.gameObject); else DestroyImmediate(contentTr.gameObject);
        contentTr = Instantiate(contentSubWinPrefab, viewportTr).transform;
        scrollRect.content = contentTr.GetComponent<RectTransform>();
    }
}
